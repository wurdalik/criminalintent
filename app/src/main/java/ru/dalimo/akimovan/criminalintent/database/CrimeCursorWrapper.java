package ru.dalimo.akimovan.criminalintent.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import ru.dalimo.akimovan.criminalintent.Crime;

import static java.util.UUID.*;

/**
 * Created by AkimovAN on 26.10.2017.
 */

public class CrimeCursorWrapper extends CursorWrapper {
    public CrimeCursorWrapper(Cursor cursor){
        super(cursor);
    }

    public Crime getCrime(){
        String uuidString = getString(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.UUID));
        String title = getString(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.TITLE));
        long date = getLong(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.DATE));
        int isSolved = getInt(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.SOLVED));
        String suspect = getString(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.SUSPECT));
        String pathPreview = getString(getColumnIndex(CrimeDbSchema.CrimeTable.Cols.PATH_PREVIEW));

        Crime crime = new Crime(fromString(uuidString));
        crime.setTitle(title);
        crime.setDate(date);
        crime.setSolved(isSolved!=0);
        crime.setSuspect(suspect);
        crime.setPathPreview(pathPreview);
        return crime;
    }


}
