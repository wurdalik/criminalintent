package ru.dalimo.akimovan.criminalintent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;
import java.util.UUID;

public class PhotoPagerActivity extends AppCompatActivity{
    private ViewPager mViewPager;
    private List<String> mPhoto;
    private UUID crimeId;
    private String mImage;
    private static final int PHOTO_PAGER = 1;
    private String path;

    @Override
    public  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_pager_activity);

        mImage = getIntent().getStringExtra("SelectImage");
        crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.CRIME_ID);

        mViewPager = findViewById(R.id.photo_preview_fragment);

        mPhoto = CrimeLab.get(this).getPath(crimeId);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position){
                path = mPhoto.get(position);
                return PhotoPagerFragment.newInstance(path, crimeId);
            }

            @Override
            public int getCount() {
                return mPhoto.size();
            }
        });

        for (int i = 0; i< mPhoto.size(); i++){
            if(mPhoto.get(i).toString().equals(mImage)){
                mViewPager.setCurrentItem(i);
                break;
            }
        }
        }


}


