package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import java.io.File;
import java.util.UUID;

public class PhotoPagerFragment extends Fragment{
    private ImageView mImageView;
    private ImageButton mOkButton;
    private String path;
    private UUID crimeId;
    String imagePath;

    public static PhotoPagerFragment newInstance(String path, UUID id){
        Bundle args = new Bundle();
        args.putString("Path", path);
        args.putSerializable(CrimeFragment.CRIME_ID, id);
        PhotoPagerFragment photoFragment = new PhotoPagerFragment();
        photoFragment.setArguments(args);
        return  photoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        path = getArguments().getString("Path");
        crimeId = (UUID)getArguments().getSerializable(CrimeFragment.CRIME_ID);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_pager, container, false);
        mImageView = v.findViewById(R.id.photo_fullscreen);
        imagePath = Environment.getExternalStorageDirectory()+path;
        File tmp = new File(imagePath);
        if (tmp.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            mImageView.setImageBitmap(PictureUtils.RotateBitmap(bitmap, 90));
        }

      mOkButton = v.findViewById(R.id.ok_button);
      mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("Path", imagePath);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            }
        });
        return v;
    }

}
