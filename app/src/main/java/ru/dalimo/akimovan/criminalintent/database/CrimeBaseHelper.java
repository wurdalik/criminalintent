package ru.dalimo.akimovan.criminalintent.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by AkimovAN on 26.10.2017.
 */

public class CrimeBaseHelper extends SQLiteOpenHelper{
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "crimeBase.db";

    public CrimeBaseHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + CrimeDbSchema.CrimeTable.NAME +
                "(" + "_id integer primary key autoincrement, " +
                CrimeDbSchema.CrimeTable.Cols.UUID+ " text, " +
                CrimeDbSchema.CrimeTable.Cols.TITLE+ " text," +
                CrimeDbSchema.CrimeTable.Cols.DATE+ " long," +
                CrimeDbSchema.CrimeTable.Cols.SOLVED+ " integer," +
                CrimeDbSchema.CrimeTable.Cols.SUSPECT+ " integer, " +
                CrimeDbSchema.CrimeTable.Cols.PATH_PREVIEW+ " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
