package ru.dalimo.akimovan.criminalintent;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;


public class CrimeListActivity extends SingleFragmentActivity  implements CrimeListFragment.Callbacks, CrimeFragment.Callbacks, CrimeFragment.IDeleteListener{
    private ImageView img;
    private TextView txt;
    private long mSysTime;

    @Override
    protected Fragment createFragment(){
        return new CrimeListFragment();
    }

    @Override
    protected int getLayoutResId(){
        return R.layout.activity_masterdetail;
    }

    @Override
    public void onCrimeSelected(Crime crime){
        img = findViewById(R.id.empty_fragment);
        img.setVisibility(View.GONE);
        txt = findViewById(R.id.txt_empty_fragment);
        txt.setVisibility(View.GONE);
        if(findViewById(R.id.detail_fragment_container)==null){
            Intent intent = CrimePagerActivity.newIntent(this, crime.getId());
            startActivity(intent);
        }
        else{
            Fragment newDetail = CrimeFragment.newInstance(crime.getId());
            getSupportFragmentManager().beginTransaction().replace(R.id.detail_fragment_container, newDetail).commit();
        }
    }

    public void onCrimeUpdated(Crime crime){
        CrimeListFragment listFragment = (CrimeListFragment)getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container);
                listFragment.updateUI();
    }

    @Override
    public void onDeleteClick() {
        CrimeListFragment fragment = (CrimeListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        img.setVisibility(View.VISIBLE);
        txt.setVisibility(View.VISIBLE);
        fragment.updateUI();
    }

    public void onBackPressed(){
        if(mSysTime+5000>System.currentTimeMillis()){
            super.onBackPressed();
        }
        else{
            Toast.makeText(getApplicationContext(), R.string.toast_exit, Toast.LENGTH_SHORT).show();
        }
            mSysTime=System.currentTimeMillis();
    }
}
