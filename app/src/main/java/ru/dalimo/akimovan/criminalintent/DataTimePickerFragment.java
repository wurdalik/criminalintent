package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * еду езще раз метод update
 * Created by AkimovAN on 05.12.2017.
 */

public class DataTimePickerFragment extends DialogFragment {
    public static final String DATE = "date";
    public static final String EXTRA_DATE = "ru.dalimo.akimovan.criminalintent.date";
    private TimePicker mTimePicker;
    private DatePicker mDatePicker;

    public static DataTimePickerFragment newInstance(Date date){
        Bundle args = new Bundle();
        args.putSerializable(DATE, date);
        DataTimePickerFragment fragment = new DataTimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Date date = (Date) getArguments().getSerializable(DATE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.date_time_layout, null);
        mDatePicker = v.findViewById(R.id.date_picker);
        mTimePicker = v.findViewById(R.id.time_picker);
        mTimePicker.setIs24HourView(true);
        mDatePicker.init(year, month, day, null);
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(R.string.date_picker_title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int year = mDatePicker.getYear();
                        int month = mDatePicker.getMonth();
                        int day = mDatePicker.getDayOfMonth();
                        int hour = mTimePicker.getCurrentHour();
                        int minute = mTimePicker.getCurrentMinute();

                        long date = new GregorianCalendar(year,month,day,hour,minute)
                                .getTimeInMillis();

                        sendResult(Activity.RESULT_OK, date);
                    }
                })
                .create();

    }
    private void sendResult(int resultCode, long date){
        if(getTargetFragment()==null){
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }
}
