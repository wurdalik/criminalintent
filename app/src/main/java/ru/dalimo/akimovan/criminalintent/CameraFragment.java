package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class CameraFragment extends Fragment {
    private File mPhoto;
    private ImageButton mCameraButton;
    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private FragmentTransaction mFragmentTransaction;
    public static final String TAG = "Tag";
    UUID mId;
    private File file;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mId=(UUID)getActivity().getIntent().getSerializableExtra(CrimeFragment.CRIME_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.camera_fragment, container, false);
        mSurfaceView = v.findViewById(R.id.surface_view);
        mCamera = Camera.open();
        SurfaceHolder holder = mSurfaceView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    mCamera.setDisplayOrientation(90);
                    mCamera.setPreviewDisplay(holder);
                    mCamera.startPreview();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                    mCamera.release();
            }
        });
        mCameraButton = v.findViewById(R.id.camera_button);

        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final File folder = new File(Environment.getExternalStorageDirectory(), "/CriminalIntent/" + mId);
                if(!folder.exists()){
                    folder.mkdirs();
                }
                String filename = mId.toString();

                for(int i =0; i<999; i++){
                    filename = mId+"_"+Integer.toString(i);
                    File candidat = new File(folder, filename + ".jpeg");
                    if (!candidat.exists()){
                        break;
                    }
                }
                file = new File(folder, filename+".jpeg");

                mCamera.takePicture(null, null, new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        try {
                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(data);
                            fos.close();

                            if(folder.listFiles().length==1){
                                Intent intent = new Intent();
                                intent.putExtra("Path", file.getAbsolutePath());
                                getActivity().setResult(Activity.RESULT_OK, intent);
                                getActivity().finish();
                            }

                            mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GalleryFragment());
                            mFragmentTransaction.commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
    }

}
