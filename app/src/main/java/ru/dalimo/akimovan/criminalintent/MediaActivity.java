package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.File;
import java.util.UUID;

import static ru.dalimo.akimovan.criminalintent.GalleryFragment.PHOTOPAGER;

public class MediaActivity extends AppCompatActivity{
    private final static String TAG = "!->MediaActivity";
    private CameraFragment mCameraFragment;
    private GalleryFragment mGalleryFragment;
    private FragmentTransaction mFragmentTransaction;
    private String fragmentName;
    private UUID crimeId;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        mCameraFragment = new CameraFragment();
        mGalleryFragment = new GalleryFragment();
        crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.CRIME_ID);
        Intent fragment = new Intent();
        fragment.putExtra(CrimeFragment.CRIME_ID, crimeId);
        File file = new File(Environment.getExternalStorageDirectory()+"/CriminalIntent/"+crimeId);
        if (!file.exists()||file.listFiles().length==0) {
            runFragment(1);
        }
        else{
            runFragment(2);
        }
    }

    public void runFragment(int nameFragment){
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (nameFragment == 1){
            mFragmentTransaction.add(R.id.fragment_container, mCameraFragment).commit();
        }
        if(nameFragment == 2){
            mFragmentTransaction.add(R.id.fragment_container, mGalleryFragment).commit();
        }
    }


}
