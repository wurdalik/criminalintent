package ru.dalimo.akimovan.criminalintent;

import java.util.Date;
import java.util.UUID;

public class Crime {
    private UUID mId;
    private String mTitle;
    private boolean mSolved;
    private Date mDate;
    private String mSuspect;
    private String mPathPreview;



    public Crime(UUID id){
        mId = id;
        mDate = new Date();
    }

    public Crime(){
        this(UUID.randomUUID());
    }

    public UUID getId() {
        return mId;
    }

    public String getSuspect() {
        return mSuspect;
    }

    public void setSuspect(String suspect) {
        mSuspect = suspect;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean isSolved() {
        return mSolved;
    }

    public void setSolved(boolean solve) {
        mSolved = solve;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = new Date(date);
    }

    public String getPhotoFilename(){
        return "IMG_" + getId().toString() + ".jpg" ;
    }

    public String getPathPreview() {
        return mPathPreview;
    }

    public void setPathPreview(String pathPreview) {
        mPathPreview = pathPreview;
    }
}
