package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TimePicker;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class CrimeFragment extends Fragment{
    private static final String ARG_CRIME_ID = "crime_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final String CAMERA = "Camera";
    private static final String GALLERY = "Gallery";
    public static final String CRIME_ID = "crime_id";
    final static String TAG_1 = "crime fragment";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_CONTACT = 1;
    private static final int REQUEST_PHOTO = 2;
    private static final int REQUEST_PHOTOVIEW = 3;

    UUID crimeId;
    private Crime mCrime;
    private EditText mTitleField;
    private Button mDateButton;
    private Button mDeleteButton;
    private Button mReportButton;
    private Button mSuspectButton;
    private ImageView mPhotoView;
    private CheckBox mSolvedCheckBox;
    private String mPhotoFile;
    private IDeleteListener mListener;
    private Callbacks mCallbacks;

    public static CrimeFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        crimeId = (UUID) getArguments().getSerializable(ARG_CRIME_ID);
        mCrime = CrimeLab.get(getActivity()).getCrime(crimeId);
//        mPhotoFile = CrimeLab.get(getActivity()).getPhotoPath(mCrime);
    }

    @Override
    public void onPause() {
        super.onPause();
        CrimeLab.get(getActivity()).updateCrime(mCrime);
    }

    public static void show(final Activity activity) {
        Runnable showingSoftKeyboard = new Runnable() {
            public void run() {
                try {
                    EditText edtText = (EditText) activity.getCurrentFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(activity.getCurrentFocus(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    edtText.setSelection(edtText.getText().length());
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Handler().postDelayed(showingSoftKeyboard, 100);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime, container, false);
        mTitleField = v.findViewById(R.id.crime_title);
        mTitleField.setText(mCrime.getTitle());
        mTitleField.setFocusable(true);
        show(getActivity());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                mCrime.setTitle(c.toString());
                updateCrime();

            }

            @Override
            public void afterTextChanged(Editable c) {
            }
        });

        mDateButton = v.findViewById(R.id.crime_data);
        updateDate();
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                DataTimePickerFragment dialog = DataTimePickerFragment.newInstance(mCrime.getDate());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }

        });

        mSolvedCheckBox = v.findViewById(R.id.crime_solved);
        mSolvedCheckBox.setChecked(false);
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCrime.setSolved(isChecked);
                updateCrime();
            }
        });

        mDeleteButton = v.findViewById(R.id.delete_crime);

        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrimeLab.get(getActivity()).delCrime(mCrime);
                mListener.onDeleteClick();
                getActivity().getSupportFragmentManager().beginTransaction().remove(CrimeFragment.this).commit();
            }
        });

        mReportButton = v.findViewById(R.id.crime_report);
        mReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, getCrimeReport());
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.crime_report_subject));
                i = Intent.createChooser(i, getString(R.string.send_report));
                startActivity(i);
            }
        });


        final Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        mSuspectButton = v.findViewById(R.id.crime_suspect);
        mSuspectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(pickContact, REQUEST_CONTACT);
            }
        });
        if (mCrime.getSuspect() != null) {
            mSuspectButton.setText(mCrime.getSuspect());
        }
        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.resolveActivity(pickContact, PackageManager.MATCH_DEFAULT_ONLY) == null) {
            mSuspectButton.setEnabled(false);
        }



        mPhotoView = v.findViewById(R.id.crime_photo);
        mPhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setPhotoView = new Intent(getActivity(), MediaActivity.class);
                setPhotoView.putExtra(CRIME_ID, mCrime.getId());
                startActivityForResult(setPhotoView, REQUEST_PHOTO);
            }
        });
        updatePhotoView();
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode){
            case REQUEST_DATE:
                long date = data.getLongExtra(DataTimePickerFragment.EXTRA_DATE, 0);
                mCrime.setDate(date);
                updateCrime();
                updateDate();
                break;
            case REQUEST_CONTACT:
                if (data != null){
                    Uri contactUri = data.getData();
                    String[] queryFields = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
                    Cursor c = getActivity().getContentResolver().query(contactUri, queryFields, null, null, null);
                    try {
                        if (c.getCount() == 0) {
                            return;
                        }
                        c.moveToFirst();
                        String suspect = c.getString(0);
                        mCrime.setSuspect(suspect);
                        updateCrime();
                        mSuspectButton.setText(suspect);
                    } finally {
                        c.close();
                    }
                }
                break;
            case REQUEST_PHOTO:
                String path = data.getStringExtra("Path");
                mCrime.setPathPreview(path);
                CrimeLab.get(getActivity()).updatePhoto(mCrime.getId(), path);
                updatePhotoView();
                updateCrime();
                break;
            case REQUEST_PHOTOVIEW:
//                updatePhotoView();
                break;
        }
    }

    @Override
    public void onResume() {
//        updatePhotoView();
        super.onResume();

    }

    private void updateDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy    HH:mm");
        String strTime = simpleDateFormat.format(mCrime.getDate());
        mDateButton.setText(strTime);
    }

    private String getCrimeReport() {
        String solvedString = null;
        if (mCrime.isSolved()) {
            solvedString = getString(R.string.crime_report_solved);
        } else {
            solvedString = getString(R.string.crime_report_unsolved);
        }

        String suspect = mCrime.getSuspect();
        if (suspect == null) {
            suspect = getString(R.string.crime_report_no_suspect);

        } else {
            suspect = getString(R.string.crime_report_suspect, suspect);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        String strTime = simpleDateFormat.format(mCrime.getDate());
        String report = getString(R.string.crime_report, mCrime.getTitle(), strTime, solvedString, suspect);
        return report;
    }

    private void updatePhotoView() {
        mPhotoFile = CrimeLab.get(getActivity()).getPhotoPath(mCrime);
//        File file = new File(Environment.getExternalStorageDirectory() + "/CriminalIntent/"+mCrime.getId());
        if (mPhotoFile == null) {
            mPhotoView.setImageResource(R.mipmap.ic_no_image);
        } else {
            Bitmap bitmap = PictureUtils.RotateBitmap(PictureUtils.getScaledBitmap(mPhotoFile, getActivity()), 90);
            mPhotoView.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks) activity;
        mListener = (IDeleteListener) activity;
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        mListener = null;

    }

    private void updateCrime() {
        CrimeLab.get(getActivity()).updateCrime(mCrime);
        mCallbacks.onCrimeUpdated(mCrime);
    }

    public interface Callbacks {
        void onCrimeUpdated(Crime crime);
    }

    public interface IDeleteListener {
        void onDeleteClick();
    }


}
