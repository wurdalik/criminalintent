package ru.dalimo.akimovan.criminalintent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.util.List;
import java.util.UUID;

public class GalleryFragment extends Fragment {
    private RecyclerView mPhotoRecyclerView;
    private PhotoAdapter mAdapter;
    private String path;
    private UUID crimeId;
    private FragmentTransaction mFragmentTransaction;
    public final static int PHOTOPAGER = 4;

    public void updatePhoto() {
        CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<String> path = crimeLab.getPath(crimeId);

        if (mAdapter == null) {
            mAdapter = new PhotoAdapter(path);
            mPhotoRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setPhoto(path);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        crimeId = (UUID) getActivity().getIntent().getSerializableExtra(CrimeFragment.CRIME_ID);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_recyclerview, container, false);
        mPhotoRecyclerView = v.findViewById(R.id.photo_recycler_view);
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 3);
        mPhotoRecyclerView.setHasFixedSize(true);
        mPhotoRecyclerView.setLayoutManager(glm);

//        mPhotoRecyclerView.addItemDecoration(new MarginDecoration(this));
        updatePhoto();
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.new_photo_button, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.new_photo_button:
                mFragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CameraFragment());
                mFragmentTransaction.commit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class PhotoAdapter extends RecyclerView.Adapter<ImageHolder>{
        private List<String> mPhoto;

        public PhotoAdapter(List<String> path) {
            mPhoto = path;
        }

        @Override
        public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.fragment_photo_recyclerview, parent, false);
            return new ImageHolder(view);
        }

        @Override
        public void onBindViewHolder(GalleryFragment.ImageHolder holder, int position) {
            path = mPhoto.get(position);
//            holder.mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.bindPicture(path);
        }

        public void setPhoto(List<String> path) {
            mPhoto = path;
        }

        public int getItemCount() {
            return mPhoto == null ? 0 : mPhoto.size();
        }
    }

    private class ImageHolder extends RecyclerView.ViewHolder{
        private ImageView mImageView;

        public ImageHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.photo_preview);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String path = (String)mImageView.getTag();
                    Intent mPhotoPreview = new Intent(getActivity(), PhotoPagerActivity.class);
                    mPhotoPreview.putExtra(CrimeFragment.CRIME_ID, crimeId);
                    mPhotoPreview.putExtra("SelectImage", path);
                    startActivityForResult(mPhotoPreview, PHOTOPAGER);
                    getActivity().setResult(Activity.RESULT_OK);
                }
            });
        }

        public void bindPicture(String fileName) {
            String path = Environment.getExternalStorageDirectory() + fileName;
            File tmp = new File(path);
            if (tmp.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(path);
                mImageView.setImageBitmap(PictureUtils.RotateBitmap(bitmap, 90));
                mImageView.setTag(fileName);
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode){
            case PHOTOPAGER:
                String path = data.getStringExtra("Path");
                Intent intent = new Intent();
                intent.putExtra("Path", path);
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
                break;
        }
    }

}



